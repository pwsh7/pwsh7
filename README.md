# pwsh7

## Get-Alias

## Get-Command

## Help
### Get-Help

- Update help

```pwsh
Update-Help -UICulture en-US
Update-Help
```

```pwsh
Get-Help about_Core_Commands
```

## Variables

```pwsh
$MaximumHistoryCount
```

- Enabling this mode causes PowerShell to report an if you violate the rules for writing code
```pwsh
Set-StrictMode -Version Latest
```

```pwsh
$color = 'blue'
```

```pswh
Set-Variable -Name color -Value blue
Get-Variable -Name color
Get-Variable -Name *Preference
```
- $null
- $true, $false

```pwsh
$color = 'red'
Select-Object -InputObject $color -Property *
$color.Length
Get-Member -InputObject $color
$color.Remove(1,1)
```

### Arrays
```pwsh
$colorPicker = @('blue','red','yellow','red')
$colorPicker[1]
$colorPicker[1..2]
$colorPicker = $colorPicker + 'orange'
$colorPicker += 'brown'
$colorPicker += @('pink','cyan')

# ArrayList
$colorPickerList = [System.Collections.ArrayList]@('blue','white','yellow','black')
$colorPickerList.Add('gray')
$colorPickerList.Remove('gray')
```

#### HashArray
```pwsh
$users = @{
abertram = 'Adam Bertram'
raquelcer = 'Raquel Cerillo'
zheng21 = 'Justin Zheng'
}
```
